﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MessageExchange
{
    public class Messenger : IMessenger
    {
        private readonly TcpClient clientPoint;

        private readonly NetworkStream Stream;
        private readonly StreamReader Reader;
        private readonly StreamWriter Writer;

        public Action<string> ReceiveMessageAction { get; set; }

        public Messenger(TcpClient clientPoint)
        {
            this.clientPoint = clientPoint;

            Stream = clientPoint.GetStream();
            Writer = new StreamWriter(Stream);
            Reader = new StreamReader(Stream);
            Writer.AutoFlush = true;
        }

        public Messenger(TcpClient clientPoint, Action<string> receiveMessageAction)
            : this(clientPoint)
        {
            ReceiveMessageAction += receiveMessageAction;
        }

        public void SendMessage(string message)
        {
            if (Writer.BaseStream != null && Writer.BaseStream.CanWrite)
            {
                Writer.WriteLine(message);
            }
            else
            {
                throw new ObjectDisposedException("Writer was null");
            }
        }

        public void ReceiveMessage()
        {
            try
            {
                if (Reader.BaseStream != null && !Reader.EndOfStream)
                {
                    ReceiveMessageAction(Reader.ReadLine());
                }
                else
                {
                    throw new ObjectDisposedException("Writer was null");
                }
            }
            catch (IOException)
            {
                EndChatting();
            }
        }

        public void EndChatting()
        {
            Reader?.Close();
            Writer?.Close();
            Stream?.Close();
            clientPoint?.Close();
        }
    }
}