﻿using System;

namespace MessageExchange
{
    public interface IMessenger
    {
        Action<string> ReceiveMessageAction { get; set; }
        void SendMessage(string message);
        void ReceiveMessage();
        void EndChatting();
    }
}