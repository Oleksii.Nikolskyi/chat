﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MessageExchange
{
    public interface IParticipant : IMessenger
    {
        protected IMessenger Messenger { get => Messenger; }
        public TaskStatus Status { get; }
        public void StartReceiving();
    }
}
