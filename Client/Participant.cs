﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MessageExchange
{
    public class Participant : IParticipant
    {
        public Action<string> ReceiveMessageAction
        {
            get
            {
                return Messenger.ReceiveMessageAction;
            }
            set
            {
                Messenger.ReceiveMessageAction = value;
            }
        }

        public TaskStatus Status => MessageReceiver.Status;

        protected IMessenger Messenger { get; }

        private readonly Task MessageReceiver;
        private readonly CancellationTokenSource cancelTokenSource;
        private readonly CancellationToken token;

        public Participant(IMessenger messenger)
        {
            Messenger = messenger;
            cancelTokenSource = new CancellationTokenSource();
            token = cancelTokenSource.Token;

            MessageReceiver = new Task(() =>
            {
                while (!token.IsCancellationRequested)
                {
                    Messenger.ReceiveMessage();
                }
            });
        }

        public void EndChatting()
        {
            cancelTokenSource.Cancel();
            Messenger.EndChatting();
        }

        public void ReceiveMessage()
        {
            Messenger.ReceiveMessage();
        }

        public void SendMessage(string message)
        {
            Messenger.SendMessage(message);
        }

        public void StartReceiving()
        { 
            MessageReceiver.Start();
        }
    }
}
