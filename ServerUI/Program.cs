﻿using System;
using Server;

namespace ServerUI
{
    class Program
    {
        static void Main(string[] args)
        {
            IAdmin admin = new Authorization("admin", "admin", new Server.Server(args[0], int.Parse(args[1])));
            admin.CreateRoom("Test Room #0");
            admin.Start();
            while (admin.Users.Count < 1) { }
            Console.WriteLine("Server ready");
            admin.Rooms[^1].AddUser(admin.Users[^1]);
            string command = string.Empty;
            while (!command.Equals("\\stop"))
            {
                command = Console.ReadLine();
            }
            admin.Stop();
        }
    }
}
