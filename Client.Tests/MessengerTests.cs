using NUnit.Framework;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System;

namespace MessageExchange.Tests
{
    [TestFixtureSource(typeof(MessengerArgsClass), "FixtureArgs")]
    public class MessengerTests
    {
        private readonly string host;
        private readonly int port;

        public MessengerTests(string Host, int Port)
        {
            host = Host;
            port = Port;
        }

        [TestCase("text with spaces")]
        [TestCase("text with special symbols !@#$%^&*()-+_/")]
        [TestCase("text")]
        [TestCase("")]
        [TestCase("a")]
        [TestCase("123456789")]
        public void SendMessage_CommonSpecialSymbolsAndDigits_ShouldBeEqual(string data)
        {
            // Arrange 
            IPAddress localAddr = IPAddress.Parse(host);
            TcpListener listener = new TcpListener(localAddr, port);
            listener.Start();

            TcpClient client = new TcpClient();
            client.Connect(host, port);
            IMessenger sender = new Messenger(client);

            TcpClient acceptedClient = listener.AcceptTcpClient();
            NetworkStream clientStream = acceptedClient.GetStream();
            StreamReader reader = new StreamReader(clientStream);

            // Act
            sender.SendMessage(data);
            string message = reader.ReadLine();

            // Assert
            Assert.AreEqual(data, message);

            listener.Stop();
            acceptedClient.Close();
            client.Close();
        }

        [TestCase("text with spaces")]
        [TestCase("text with special symbols !@#$%^&*()-+_/")]
        [TestCase("text")]
        [TestCase("")]
        [TestCase("a")]
        [TestCase("123456789")]
        public void ReceiveMessage_CommonSpecialSymbolsAndDigits_ShouldBeEqual(string data)
        {
            // Arrange 
            IPAddress localAddr = IPAddress.Parse(host);
            TcpListener listener = new TcpListener(localAddr, port);
            listener.Start();

            TcpClient client = new TcpClient();
            client.Connect(host, port);
            string message = string.Empty;
            IMessenger receiver = new Messenger(client, (string str) => { message = str; });
            
            TcpClient acceptedClient = listener.AcceptTcpClient();
            NetworkStream clientStream = acceptedClient.GetStream();
            StreamWriter writer = new StreamWriter(clientStream) { AutoFlush = true };

            // Act
            writer.WriteLine(data);
            receiver.ReceiveMessage();

            // Assert
            Assert.AreEqual(data, message);

            writer.Close();
            acceptedClient.Close();
            client.Close();
            listener.Stop();
        }

        [TestCase("text")]
        public void SendMessage_EndChatting_ShouldThrowIOException(string data)
        {
            // Arrange 
            IPAddress localAddr = IPAddress.Parse(host);
            TcpListener listener = new TcpListener(localAddr, port);
            listener.Start();

            TcpClient client = new TcpClient();
            client.Connect(host, port);
            IMessenger sender = new Messenger(client);

            TcpClient acceptedClient = listener.AcceptTcpClient();
            NetworkStream clientStream = acceptedClient.GetStream();
            StreamReader reader = new StreamReader(clientStream);

            var expectedEx = typeof(ObjectDisposedException);

            // Act
            sender.SendMessage(data);
            string message = reader.ReadLine();
            sender.EndChatting();
            var actEx = Assert.Catch(() => sender.SendMessage(data));

            // Assert
            Assert.AreEqual(expectedEx, actEx.GetType());

            listener.Stop();
            acceptedClient.Close();
            client.Close();
        }

        [TestCase("text")]
        public void ReceiveMessage_EndChatting_ShouldThrowIOException(string data)
        {
            // Arrange 
            IPAddress localAddr = IPAddress.Parse(host);
            TcpListener listener = new TcpListener(localAddr, port);
            listener.Start();

            TcpClient client = new TcpClient();
            client.Connect(host, port);
            string message = string.Empty;
            IMessenger receiver = new Messenger(client, (string str) => { message = str; });

            TcpClient acceptedClient = listener.AcceptTcpClient();
            NetworkStream clientStream = acceptedClient.GetStream();
            StreamWriter writer = new StreamWriter(clientStream) { AutoFlush = true };

            var expectedEx = typeof(ObjectDisposedException);

            // Act
            writer.WriteLine(data);
            receiver.ReceiveMessage();
            receiver.EndChatting();
            var actEx = Assert.Catch(() => receiver.ReceiveMessage());

            // Assert
            Assert.AreEqual(expectedEx, actEx.GetType());

            writer.Close();
            acceptedClient.Close();
            client.Close();
            listener.Stop();
        }
    }

    internal class MessengerArgsClass
    {
        static object[] FixtureArgs = { new object[] { "127.0.0.1", 1234 } };
    }
}