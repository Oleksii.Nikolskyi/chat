﻿using NUnit.Framework;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace MessageExchange.Tests
{
    [TestFixtureSource(typeof(ParticipantArgsClass), "FixtureArgs")]
    public class ParticipantTests
    {
        private readonly List<string> expected;
        private readonly string host;
        private readonly int port;

        public ParticipantTests(List<string> data, string Host, int Port)
        {
            expected = data;
            host = Host;
            port = Port;
        }

        [Test]
        public void TaskShouldReceiveCorrectData()
        {
            // Arrange 
            TcpListener listener = new TcpListener(IPAddress.Parse(host), port);
            listener.Start();
            TcpClient client = new TcpClient(host, port);

            List<string> messages = new List<string>();
            IMessenger receiver = new Messenger(client, (string str) => { messages.Add(str); });
            IParticipant participant = new Participant(receiver);

            TcpClient acceptedClient = listener.AcceptTcpClient();
            StreamWriter writer = new StreamWriter(acceptedClient.GetStream()) { AutoFlush = true };

            // Act
            participant.StartReceiving();
            for (int i = 0; i < expected.Count; i++)
            {
                writer.WriteLine(expected[i]);
                Thread.Sleep(50);
            }

            // Assert
            Assert.IsTrue(messages.SequenceEqual(expected));

            writer.Close();
            acceptedClient.Close();
            participant.EndChatting();
            receiver.EndChatting();
            client.Close();
            listener.Stop();
        }

        [Test]
        public void TaskShouldRun()
        {
            // Arrange 
            IPAddress localAddr = IPAddress.Parse(host);
            TcpListener listener = new TcpListener(localAddr, port);
            listener.Start();

            TcpClient client = new TcpClient();
            client.Connect(host, port);
            List<string> messages = new List<string>();
            IMessenger receiver = new Messenger(client, (string str) => { messages.Add(str); });
            IParticipant participant = new Participant(receiver);

            TcpClient acceptedClient = listener.AcceptTcpClient();
            NetworkStream clientStream = acceptedClient.GetStream();
            StreamWriter writer = new StreamWriter(clientStream) { AutoFlush = true };

            // Act
            participant.StartReceiving();
            Thread.Sleep(100);

            // Assert
            Assert.IsTrue(participant.Status == System.Threading.Tasks.TaskStatus.Running);

            participant.EndChatting();
            receiver.EndChatting();
            writer.Close();
            clientStream.Close();
            acceptedClient.Close();
            client.Close();
            listener.Stop();
        }

        [Test]
        public void TaskShouldStop()
        {
            // Arrange 
            IPAddress localAddr = IPAddress.Parse(host);
            TcpListener listener = new TcpListener(localAddr, port);
            listener.Start();

            TcpClient client = new TcpClient();
            client.Connect(host, port);
            IMessenger receiver = new Messenger(client);
            IParticipant participant = new Participant(receiver);

            TcpClient acceptedClient = listener.AcceptTcpClient();
            NetworkStream clientStream = acceptedClient.GetStream();
            StreamWriter writer = new StreamWriter(clientStream) { AutoFlush = true };

            // Act
            participant.StartReceiving();
            Thread.Sleep(100);

            participant.EndChatting();
            Thread.Sleep(100);

            // Assert
            Assert.IsTrue(participant.Status == System.Threading.Tasks.TaskStatus.RanToCompletion);

            writer.Close();
            clientStream.Close();
            acceptedClient.Close();
            client.Close();
            listener.Stop();
        }

        /*
         * ! Bottom tests were written to check aggregation between Participant and Messenger
         */

        [TestCase("text with spaces")]
        [TestCase("text with special symbols !@#$%^&*()-+_/")]
        [TestCase("text")]
        [TestCase("")]
        [TestCase("a")]
        [TestCase("123456789")]
        public void SendMessage_CommonSpecialSymbolsAndDigits_ShouldBeEqual(string data)
        {
            // Arrange 
            IPAddress localAddr = IPAddress.Parse(host);
            TcpListener listener = new TcpListener(localAddr, port);
            listener.Start();

            TcpClient client = new TcpClient();
            client.Connect(host, port);
            IMessenger sender = new Messenger(client);
            IParticipant participant = new Participant(sender);

            TcpClient acceptedClient = listener.AcceptTcpClient();
            NetworkStream clientStream = acceptedClient.GetStream();
            StreamReader reader = new StreamReader(clientStream);

            // Act
            participant.SendMessage(data);
            string message = reader.ReadLine();

            // Assert
            Assert.AreEqual(data, message);

            listener.Stop();
            acceptedClient.Close();
            client.Close();
        }

        [TestCase("text with spaces")]
        [TestCase("text with special symbols !@#$%^&*()-+_/")]
        [TestCase("text")]
        [TestCase("")]
        [TestCase("a")]
        [TestCase("123456789")]
        public void ReceiveMessage_CommonSpecialSymbolsAndDigits_ShouldBeEqual(string data)
        {
            // Arrange 
            IPAddress localAddr = IPAddress.Parse(host);
            TcpListener listener = new TcpListener(localAddr, port);
            listener.Start();

            TcpClient client = new TcpClient();
            client.Connect(host, port);
            string message = string.Empty;
            IMessenger receiver = new Messenger(client, (string str) => { message = str; });
            IParticipant participant = new Participant(receiver);

            TcpClient acceptedClient = listener.AcceptTcpClient();
            NetworkStream clientStream = acceptedClient.GetStream();
            StreamWriter writer = new StreamWriter(clientStream) { AutoFlush = true };

            // Act
            writer.WriteLine(data);
            participant.ReceiveMessage();

            // Assert
            Assert.AreEqual(data, message);

            writer.Close();
            acceptedClient.Close();
            client.Close();
            listener.Stop();
        }

        [TestCase("text")]
        public void SendMessage_EndChatting_ShouldThrowIOException(string data)
        {
            // Arrange 
            IPAddress localAddr = IPAddress.Parse(host);
            TcpListener listener = new TcpListener(localAddr, port);
            listener.Start();

            TcpClient client = new TcpClient();
            client.Connect(host, port);
            IMessenger sender = new Messenger(client);
            IParticipant participant = new Participant(sender);

            TcpClient acceptedClient = listener.AcceptTcpClient();
            NetworkStream clientStream = acceptedClient.GetStream();
            StreamReader reader = new StreamReader(clientStream);

            var expectedEx = typeof(ObjectDisposedException);

            // Act
            participant.SendMessage(data);
            string message = reader.ReadLine();
            participant.EndChatting();
            var actEx = Assert.Catch(() => participant.SendMessage(data));

            // Assert
            Assert.AreEqual(expectedEx, actEx.GetType());

            listener.Stop();
            acceptedClient.Close();
            client.Close();
        }

        [TestCase("text")]
        public void ReceiveMessage_EndChatting_ShouldThrowIOException(string data)
        {
            // Arrange 
            IPAddress localAddr = IPAddress.Parse(host);
            TcpListener listener = new TcpListener(localAddr, port);
            listener.Start();

            TcpClient client = new TcpClient();
            client.Connect(host, port);
            string message = string.Empty;
            IMessenger receiver = new Messenger(client, (string str) => { message = str; });
            IParticipant participant = new Participant(receiver);

            TcpClient acceptedClient = listener.AcceptTcpClient();
            NetworkStream clientStream = acceptedClient.GetStream();
            StreamWriter writer = new StreamWriter(clientStream) { AutoFlush = true };

            var expectedEx = typeof(ObjectDisposedException);

            // Act
            writer.WriteLine(data);
            participant.ReceiveMessage();
            participant.EndChatting();
            var actEx = Assert.Catch(() => participant.ReceiveMessage());

            // Assert
            Assert.AreEqual(expectedEx, actEx.GetType());

            writer.Close();
            acceptedClient.Close();
            client.Close();
            listener.Stop();
        }
    }

    internal class ParticipantArgsClass
    {
        static object[] FixtureArgs = { new object[] { new List<string>() { "test", "1234", "!@#$%^&*()-=+/", "aa bb", "", "a" }, "127.0.0.1", 1234 } };
    }
}
