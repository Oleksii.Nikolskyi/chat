﻿using MessageExchange;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;

namespace Server
{
    public class Room : IRoom
    {
        public ReadOnlyCollection<IMessenger> Participants => clients.AsReadOnly();
        
        private readonly List<IMessenger> clients;

        public Room(string name)
        {
            Name = name;
            clients = new List<IMessenger>();
        }

        public string Name { get; set; }

        public void AddUser(IMessenger participant)
        {
            participant.ReceiveMessageAction += Mailing;
            clients.Add(participant);
        }

        public void RemoveUser(IMessenger participant)
        {
            participant.ReceiveMessageAction -= Mailing;
            if(!clients.Remove(participant))
            {
                throw new System.ApplicationException($"Participant doesn't exist!");
            }
        }

        public void Mailing(string message)
        {
            foreach (IMessenger _p in clients)
            {
                _p.SendMessage(message);
            }
        }
    }
}