﻿using MessageExchange;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Authorization : IAuthorization
    {
        protected IAdmin ServerAdmin { get; private set; }

        public string Login { get; } = "admin";

        public string Password { get; } = "admin";

        public ReadOnlyCollection<IRoom> Rooms => ServerAdmin.Rooms;

        public ReadOnlyCollection<IParticipant> Users => ServerAdmin.Users;

        public TaskStatus Status => ServerAdmin.Status;

        public Authorization(string login, string password, IServer server)
        {
            if (Login.Equals(login) && Password.Equals(password))
            {
                ServerAdmin = new Admin(server);
            }
            else
            {
                throw new ApplicationException("Authorization failed");
            }
        }

        public void ConnectClient()
        {
            ServerAdmin.ConnectClient();
        }

        public void CreateRoom(string name)
        {
            ServerAdmin.CreateRoom(name);
        }

        public void DeleteRoom(string name)
        {
            ServerAdmin.DeleteRoom(name);
        }

        public void DisconnectClient(TcpClient client)
        {
            ServerAdmin.DisconnectClient(client);
        }

        public void Start()
        {
            ServerAdmin.Start();
        }

        public void Stop()
        {
            ServerAdmin.Stop();
        }
    }
}
