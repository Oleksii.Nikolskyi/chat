﻿using MessageExchange;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Server
{
    public class Admin : IAdmin
    {
        private readonly List<IRoom> rooms;
        protected IServer Server { get; private set; }
        public ReadOnlyCollection<IRoom> Rooms => rooms.AsReadOnly();

        public ReadOnlyCollection<IParticipant> Users => Server.Users;

        public TaskStatus Status => Server.Status;

        public Admin(IServer server)
        {
            Server = server;
            rooms = new List<IRoom>();
        }

        public void ConnectClient()
        {
            Server.ConnectClient();
        }

        public void DisconnectClient(TcpClient client)
        {
            Server.DisconnectClient(client);
        }

        public void CreateRoom(string name)
        {
            IRoom _room = new Room(name);
            rooms.Add(_room);
        }

        public void DeleteRoom(string name)
        {
            if(!rooms.Remove(rooms.Find(r => r.Name.Equals(name))))
            {
                throw new System.ApplicationException($"Room {name} doesn't exist!");
            }
        }

        public void Start()
        {
            Server.Start();
        }

        public void Stop()
        {
            Server.Stop();
        }
    }
}