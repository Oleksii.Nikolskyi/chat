﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using MessageExchange;

namespace Server
{
    public interface IRoom
    {
        public string Name { get; set; }
        public ReadOnlyCollection<IMessenger> Participants { get; }
        public void AddUser(IMessenger user);
        public void RemoveUser(IMessenger user);
        public void Mailing(string message);
    }
}
