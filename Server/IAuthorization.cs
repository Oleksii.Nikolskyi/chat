﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Server
{
    public interface IAuthorization : IAdmin
    {
        protected IAdmin ServerAdmin { get => ServerAdmin; }
        public string Login { get => Login; }
        public string Password { get => Password; }
    }
}
