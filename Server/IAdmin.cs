﻿using System.Collections.ObjectModel;

namespace Server
{
    public interface IAdmin : IServer
    {
        protected IServer Server { get => Server; }
        public ReadOnlyCollection<IRoom> Rooms { get; }
        public void CreateRoom(string name);
        public void DeleteRoom(string name);
    }
}
