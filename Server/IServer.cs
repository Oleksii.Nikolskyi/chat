﻿using MessageExchange;
using System.Collections.ObjectModel;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Server
{
    public interface IServer
    {
        public ReadOnlyCollection<IParticipant> Users { get; }
        public TaskStatus Status { get; }
        void ConnectClient();
        void DisconnectClient(TcpClient client);
        void Start();
        void Stop();
    }
}