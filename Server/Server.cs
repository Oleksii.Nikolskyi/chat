﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using MessageExchange;

namespace Server
{
    public class Server : IServer
    {
        private readonly List<IParticipant> users;
        private readonly TcpListener serverPoint;

        private readonly CancellationTokenSource cancelTokenSource;
        private readonly CancellationToken token;

        private readonly Task acceptingClients;

        public ReadOnlyCollection<IParticipant> Users => users.AsReadOnly();
        public TaskStatus Status => acceptingClients.Status;

        public Server(string host, int port)
        {
            IPAddress localAddr = IPAddress.Parse(host);
            serverPoint = new TcpListener(localAddr, port);
            serverPoint.Start();

            users = new List<IParticipant>();

            cancelTokenSource = new CancellationTokenSource();
            token = cancelTokenSource.Token;

            acceptingClients = new Task(() =>
            {
                while (!token.IsCancellationRequested)
                {
                    ConnectClient();
                }
            });
        }

        public void ConnectClient()
        {
            TcpClient newClientPoint = null;
            IMessenger client = null;
            IParticipant participant = null;
            try
            {
                newClientPoint = serverPoint.AcceptTcpClient();
                client = new Messenger(newClientPoint);
                participant = new Participant(client);
                participant.StartReceiving();
                users.Add(participant);
            }
            catch (InvalidOperationException ex)
            {
                throw ex;
            }
            catch (SocketException ex)
            {
                participant?.EndChatting();
            }
        }

        public void DisconnectClient(TcpClient client)
        {
            try
            {
                client.GetStream().Close();
            }
            catch (ObjectDisposedException ex)
            {
                throw ex;
            }
            finally
            {
                client.Close();
            }
        }

        public void Start()
        {
            acceptingClients.Start();
        }

        public void Stop()
        {
            cancelTokenSource.Cancel();
            try
            {
                serverPoint.Stop();
            }
            catch (SocketException ex)
            {
                throw ex;
            }
        }
    }
}