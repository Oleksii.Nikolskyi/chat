﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Server;

namespace ServerGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IAuthorization server;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void connectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                server = new Authorization(loginTextBox.Text, passwordBox.Password,
                    new Server(hostTextBox.Text, int.Parse(portTextBox.Text)));
            }catch(ApplicationException ex)
            {
                logListBox.Items.Add(ex.Message);
            }
            logListBox.Items.Add($"Connected successfully");
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            server?.Start();
        }

        private void createRoomButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void deleteRoomButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void disconnectUserButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            server?.Stop();
        }
    }
}
