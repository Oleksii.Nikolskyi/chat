﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Server;

namespace AdminApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IAuthorization server = null;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void connectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                server = new Authorization(loginTextBox.Text, passwordBox.Password,
                    new Server.Server(hostTextBox.Text, int.Parse(portTextBox.Text)));
                logListBox.Items.Add($"{DateTime.Now}: Connected successfully");
            }
            catch (ApplicationException ex)
            {
                logListBox.Items.Add(ex.Message);
            }
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                server.Start();
                if (server.Status == TaskStatus.Running)
                {
                    logListBox.Items.Add($"{DateTime.Now}: Listening started");
                }
            }
            catch (Exception ex)
            {
                logListBox.Items.Add(ex.Message);
            }
        }

        private void createRoomButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                server.CreateRoom($"Room #{DateTime.Now}".Trim());
            }
            catch (ApplicationException ex)
            {
                logListBox.Items.Add(ex.Message);
            }
        }

        private void deleteRoomButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void disconnectUserButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                server.Stop();
                if (server.Status != TaskStatus.RanToCompletion)
                {
                    logListBox.Items.Add($"{DateTime.Now}: Listening stopped");
                }
            }
            catch (Exception ex)
            {
                logListBox.Items.Add(ex.Message);
            }
        }

        private void usersUpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                usersListBox.Items.Clear();
                foreach (var user in server.Users)
                {
                    usersListBox.Items.Add(user.ToString());
                }
            }
            catch (Exception ex)
            {
                logListBox.Items.Add(ex.Message);
            }
        }

        private void roomsUpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                roomsListBox.Items.Clear();
                foreach (var room in server.Rooms)
                {
                    roomsListBox.Items.Add(room.Name);
                }
            }
            catch (Exception ex)
            {
                logListBox.Items.Add(ex.Message);
            }
        }
    }
}
