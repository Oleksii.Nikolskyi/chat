﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using MessageExchange;
using NUnit.Framework;

namespace Server.Tests
{
    [TestFixtureSource(typeof(AdminArgsClass), "FixtureArgs")]
    class AdminTests
    {
        private readonly string host;
        private readonly int port;

        public AdminTests(string Host, int Port)
        {
            host = Host;
            port = Port;
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(10)]
        [TestCase(50)]
        public void RoomsProperty_ShouldReturnSameCount(int expectedCount)
        {
            // Arrange
            IServer server = new Server(host, port);
            IAdmin admin = new Admin(server);

            // Act
            for (int i = 0; i < expectedCount; i++)
            {
                admin.CreateRoom($"Test Room #{i}");
            }

            // Assert
            Assert.AreEqual(expectedCount, admin.Rooms.Count);

            admin.Stop();
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(10)]
        [TestCase(50)]
        public void RoomsProperty_ShouldReturnSameNames(int expectedCount)
        {
            // Arrange
            IServer server = new Server(host, port);
            IAdmin admin = new Admin(server);

            // Act
            for (int i = 0; i < expectedCount; i++)
            {
                admin.CreateRoom($"Test Room #{i}");
            }

            // Assert
            Assert.AreEqual(expectedCount, admin.Rooms.Count);

            for(int j = 0; j < admin.Rooms.Count; j++)
            {
                Assert.AreEqual($"Test Room #{j}", admin.Rooms[j].Name);
            }

            admin.Stop();
        }

        [Test]
        public void DeleteRoom_ShouldThrowApplicationException()
        {
            // Arrange
            IServer server = new Server(host, port);
            IAdmin admin = new Admin(server);
            var expectedEx = typeof(ApplicationException);

            // Act
            var actEx = Assert.Catch(() => admin.DeleteRoom("Test Room #0"));

            // Assert
            Assert.AreEqual(expectedEx, actEx.GetType());

            admin.Stop();
        }


        [TestCase(1, 0)]
        [TestCase(10, 5)]
        [TestCase(50, 25)]
        public void DeleteRoom_ShouldRemoveNeededRooms_AndReturnCorrectRoomsCount(int roomsCountBefore, int roomsCountAfter)
        {
            // Arrange
            IServer server = new Server(host, port);
            IAdmin admin = new Admin(server);

            for (int i = 0; i < roomsCountBefore; i++)
            {
                admin.CreateRoom($"Test Room #{i}");
            }

            // Act
            for(int i = 0; i < roomsCountBefore; i += 2)
            {
                admin.DeleteRoom($"Test Room #{i}");
            }

            // Assert
            Assert.AreEqual(roomsCountAfter, admin.Rooms.Count);

            for (int i = 1, j = 0; j < admin.Rooms.Count; i += 2, j++)
            {
                Assert.AreEqual($"Test Room #{i}", admin.Rooms[j].Name);
            }

            admin.Stop();
        }

        /*
         * ! Bottom tests were written to check aggregation between Admin and Server
         */

        [Test]
        public void Stop_ServerShouldDisposeTcpListenerResources()
        {
            // Arrange
            IServer server = new Server(host, port);
            IAdmin admin = new Admin(server);
            var expectedEx = typeof(InvalidOperationException);

            string testMessage = string.Empty;
            TcpClient clientPoint = new TcpClient(host, port);
            IMessenger messenger = new Messenger(clientPoint, (string s) => { testMessage = s; });
            IParticipant participant = new Participant(messenger);

            // Act
            admin.Stop();
            var actualEx = Assert.Catch(() => admin.ConnectClient());

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType());
        }

        [Test]
        public void Start_TaskShouldRunning()
        {
            // Arrange
            IServer server = new Server(host, port);
            IAdmin admin = new Admin(server);

            // Act
            admin.Start();
            Thread.Sleep(50);

            // Asert
            Assert.IsTrue(admin.Status == System.Threading.Tasks.TaskStatus.Running);
            admin.Stop();
        }

        [Test]
        public void Stop_TaskShouldBeRanToCompletion()
        {
            // Arrange
            IServer server = new Server(host, port);
            IAdmin admin = new Admin(server);

            // Act
            admin.Start();
            admin.Stop();
            Thread.Sleep(50);

            // Asert
            Assert.IsTrue(admin.Status == System.Threading.Tasks.TaskStatus.RanToCompletion);
        }

        [Test]
        public void DisconnectClient_SecondCallShouldThrowObjectDisposedException()
        {
            // Arrange
            IServer server = new Server(host, port);
            IAdmin admin = new Admin(server);
            string testMessage = string.Empty;
            var expectedEx = typeof(ObjectDisposedException);
            TcpClient clientPoint = new TcpClient(host, port);
            IMessenger messenger = new Messenger(clientPoint, (string s) => { testMessage = s; });
            IParticipant participant = new Participant(messenger);

            // Act
            admin.Start();
            admin.DisconnectClient(clientPoint);
            var actualEx = Assert.Catch(() => admin.DisconnectClient(clientPoint));

            // Asert
            Assert.AreEqual(expectedEx, actualEx.GetType());

            admin.Stop();
        }

        [Test]
        public void ConnectClient_ServerShouldGetConnection()
        {
            // Arrange
            IServer server = new Server(host, port);
            IAdmin admin = new Admin(server);
            var expectedEx = typeof(InvalidOperationException);

            string testMessage = string.Empty;
            TcpClient clientPoint = new TcpClient(host, port);
            IMessenger messenger = new Messenger(clientPoint, (string s) => { testMessage = s; });
            IParticipant participant = new Participant(messenger);

            // Act
            admin.ConnectClient();

            // Assert
            Assert.AreEqual(1, admin.Users.Count);
            Assert.AreEqual(participant.GetType(), admin.Users[^1].GetType());

            admin.Stop();
        }


        [TestCase(0)]
        [TestCase(1)]
        [TestCase(3)]
        [TestCase(10)]
        [TestCase(40)]
        public void Start_ShouldAddAllClientsToList(int expectedCount)
        {
            // Arrange
            IServer server = new Server(host, port);
            IAdmin admin = new Admin(server);
            string testMessage = string.Empty;

            // Act
            admin.Start();

            for (int i = 0; i < expectedCount; i++)
            {
                TcpClient clientPoint = new TcpClient(host, port);
                IMessenger messenger = new Messenger(clientPoint, (string s) => { testMessage = s; });
                IParticipant participant = new Participant(messenger);
                Thread.Sleep(50);
            }

            // Assert
            Assert.AreEqual(expectedCount, admin.Users.Count);
            admin.Stop();
        }

        [Test]
        public void DisconnectClient_ShouldCloseClientSocket()
        {
            // Arrange
            IServer server = new Server(host, port);
            IAdmin admin = new Admin(server);
            string testMessage = string.Empty;
            TcpClient clientPoint = new TcpClient(host, port);
            IMessenger messenger = new Messenger(clientPoint, (string s) => { testMessage = s; });
            IParticipant participant = new Participant(messenger);

            // Act
            admin.Start();
            admin.DisconnectClient(clientPoint);

            // Assert
            Assert.IsFalse(clientPoint.Connected);

            admin.Stop();
        }
    }

    internal class AdminArgsClass
    {
        static object[] FixtureArgs = { new object[] { "127.0.0.1", 1234 } };
    }
}
