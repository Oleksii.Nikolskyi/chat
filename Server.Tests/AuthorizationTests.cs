using System;
using System.Net.Sockets;
using System.Threading;
using MessageExchange;
using NUnit.Framework;

namespace Server.Tests
{   
    [TestFixtureSource(typeof(AuthorizationArgsClass), "FixtureArgs")]
    class AuthorizationTests
    {
        private readonly string host;
        private readonly int port;

        public AuthorizationTests(string Host, int Port)
        {
            host = Host;
            port = Port;
        }

        [TestCase("authorization", "123")]
        [TestCase("123", "authorization")]
        public void AuthorizationCtor_WrongLogin_ShouldThrowException(string login, string password)
        {
            // Arrange
            IServer server = new Server(host, port);
            IAuthorization authorization = null;

            // Act
            ApplicationException actEx = Assert.Throws<ApplicationException>(() => new Authorization(login, password, server));

            // Assert
            Assert.That(actEx.Message.Equals("Authorization failed"));

            if (authorization == null)
            {
                server.Stop();
            }
            else
            {
                authorization.Stop();
            }
        }
        
        /*
         * ! Bottom tests were written to check aggregation between Authorization and Admin
         */

        [TestCase("admin", "admin", 0)]
        [TestCase("admin", "admin", 1)]
        [TestCase("admin", "admin", 10)]
        [TestCase("admin", "admin", 50)]
        public void RoomsProperty_ShouldReturnSameCount(string login, string password, int expectedCount)
        {
            // Arrange
            IServer server = new Server(host, port);
            IAuthorization authorization = new Authorization(login, password, server);

            // Act
            for (int i = 0; i < expectedCount; i++)
            {
                authorization.CreateRoom($"Test Room #{i}");
            }

            // Assert
            Assert.AreEqual(expectedCount, authorization.Rooms.Count);

            authorization.Stop();
        }

        [TestCase("admin", "admin", 0)]
        [TestCase("admin", "admin", 1)]
        [TestCase("admin", "admin", 10)]
        [TestCase("admin", "admin", 50)]
        public void RoomsProperty_ShouldReturnSameNames(string login, string password, int expectedCount)
        {
            // Arrange
            IServer server = new Server(host, port);
            IAuthorization authorization = new Authorization(login, password, server);

            // Act
            for (int i = 0; i < expectedCount; i++)
            {
                authorization.CreateRoom($"Test Room #{i}");
            }

            // Assert
            Assert.AreEqual(expectedCount, authorization.Rooms.Count);

            for (int j = 0; j < authorization.Rooms.Count; j++)
            {
                Assert.AreEqual($"Test Room #{j}", authorization.Rooms[j].Name);
            }

            authorization.Stop();
        }

        [TestCase("admin", "admin")]
        public void DeleteRoom_ShouldThrowApplicationException(string login, string password)
        {
            // Arrange
            IServer server = new Server(host, port);
            IAuthorization authorization = new Authorization(login, password, server);
            var expectedEx = typeof(ApplicationException);

            // Act
            var actEx = Assert.Catch(() => authorization.DeleteRoom("Test Room #0"));

            // Assert
            Assert.AreEqual(expectedEx, actEx.GetType());

            authorization.Stop();
        }


        [TestCase("admin", "admin", 1, 0)]
        [TestCase("admin", "admin", 10, 5)]
        [TestCase("admin", "admin", 50, 25)]
        public void DeleteRoom_ShouldRemoveNeededRooms_AndReturnCorrectRoomsCount(string login, string password, int roomsCountBefore, int roomsCountAfter)
        {
            // Arrange
            IServer server = new Server(host, port);
            IAuthorization authorization = new Authorization(login, password, server);

            for (int i = 0; i < roomsCountBefore; i++)
            {
                authorization.CreateRoom($"Test Room #{i}");
            }

            // Act
            for (int i = 0; i < roomsCountBefore; i += 2)
            {
                authorization.DeleteRoom($"Test Room #{i}");
            }

            // Assert
            Assert.AreEqual(roomsCountAfter, authorization.Rooms.Count);

            for (int i = 1, j = 0; j < authorization.Rooms.Count; i += 2, j++)
            {
                Assert.AreEqual($"Test Room #{i}", authorization.Rooms[j].Name);
            }

            authorization.Stop();
        }


        [TestCase("admin", "admin")]
        public void Stop_ServerShouldDisposeTcpListenerResources(string login, string password)
        {
            // Arrange
            IServer server = new Server(host, port);
            IAuthorization authorization = new Authorization(login, password, server);
            var expectedEx = typeof(InvalidOperationException);

            string testMessage = string.Empty;
            TcpClient clientPoint = new TcpClient(host, port);
            IMessenger messenger = new Messenger(clientPoint, (string s) => { testMessage = s; });
            IParticipant participant = new Participant(messenger);

            // Act
            authorization.Stop();
            var actualEx = Assert.Catch(() => authorization.ConnectClient());

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType());
        }

        [TestCase("admin", "admin")]
        public void Start_TaskShouldRunning(string login, string password)
        {
            // Arrange
            IServer server = new Server(host, port);
            IAuthorization authorization = new Authorization(login, password, server);

            // Act
            authorization.Start();
            Thread.Sleep(50);

            // Asert
            Assert.IsTrue(authorization.Status == System.Threading.Tasks.TaskStatus.Running);
            authorization.Stop();
        }

        [TestCase("admin", "admin")]
        public void Stop_TaskShouldBeRanToCompletion(string login, string password)
        {
            // Arrange
            IServer server = new Server(host, port);
            IAuthorization authorization = new Authorization(login, password, server);

            // Act
            authorization.Start();
            authorization.Stop();
            Thread.Sleep(50);

            // Asert
            Assert.IsTrue(authorization.Status == System.Threading.Tasks.TaskStatus.RanToCompletion);
        }

        [TestCase("admin", "admin")]
        public void DisconnectClient_SecondCallShouldThrowObjectDisposedException(string login, string password)
        {
            // Arrange
            IServer server = new Server(host, port);
            IAuthorization authorization = new Authorization(login, password, server);
            string testMessage = string.Empty;
            var expectedEx = typeof(ObjectDisposedException);
            TcpClient clientPoint = new TcpClient(host, port);
            IMessenger messenger = new Messenger(clientPoint, (string s) => { testMessage = s; });
            IParticipant participant = new Participant(messenger);

            // Act
            authorization.Start();
            authorization.DisconnectClient(clientPoint);
            var actualEx = Assert.Catch(() => authorization.DisconnectClient(clientPoint));

            // Asert
            Assert.AreEqual(expectedEx, actualEx.GetType());

            authorization.Stop();
        }

        [TestCase("admin", "admin")]
        public void ConnectClient_ServerShouldGetConnection(string login, string password)
        {
            // Arrange
            IServer server = new Server(host, port);
            IAuthorization authorization = new Authorization(login, password, server);
            var expectedEx = typeof(InvalidOperationException);

            string testMessage = string.Empty;
            TcpClient clientPoint = new TcpClient(host, port);
            IMessenger messenger = new Messenger(clientPoint, (string s) => { testMessage = s; });
            IParticipant participant = new Participant(messenger);

            // Act
            authorization.ConnectClient();

            // Assert
            Assert.AreEqual(1, authorization.Users.Count);
            Assert.AreEqual(participant.GetType(), authorization.Users[^1].GetType());

            authorization.Stop();
        }


        [TestCase("admin", "admin", 0)]
        [TestCase("admin", "admin", 1)]
        [TestCase("admin", "admin", 3)]
        [TestCase("admin", "admin", 10)]
        [TestCase("admin", "admin", 40)]
        public void Start_ShouldAddAllClientsToList(string login, string password, int expectedCount)
        {
            // Arrange
            IServer server = new Server(host, port);
            IAuthorization authorization = new Authorization(login, password, server);
            string testMessage = string.Empty;

            // Act
            authorization.Start();

            for (int i = 0; i < expectedCount; i++)
            {
                TcpClient clientPoint = new TcpClient(host, port);
                IMessenger messenger = new Messenger(clientPoint, (string s) => { testMessage = s; });
                IParticipant participant = new Participant(messenger);
                Thread.Sleep(50);
            }

            // Assert
            Assert.AreEqual(expectedCount, authorization.Users.Count);
            authorization.Stop();
        }

        [TestCase("admin", "admin")]
        public void DisconnectClient_ShouldCloseClientSocket(string login, string password)
        {
            // Arrange
            IServer server = new Server(host, port);
            IAuthorization authorization = new Authorization(login, password, server);
            string testMessage = string.Empty;
            TcpClient clientPoint = new TcpClient(host, port);
            IMessenger messenger = new Messenger(clientPoint, (string s) => { testMessage = s; });
            IParticipant participant = new Participant(messenger);

            // Act
            authorization.Start();
            authorization.DisconnectClient(clientPoint);

            // Assert
            Assert.IsFalse(clientPoint.Connected);

            authorization.Stop();
        }
    }

    internal class AuthorizationArgsClass
    {
        static object[] FixtureArgs = { new object[] { "127.0.0.1", 1234 } };
    }
}