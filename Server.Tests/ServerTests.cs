using MessageExchange;
using NUnit.Framework;
using System;
using System.Net.Sockets;
using System.Threading;

namespace Server.Tests
{
    [TestFixtureSource(typeof(ServerArgsClass), "FixtureArgs")]
    public class ServerTests
    {
        private readonly string host;
        private readonly int port;

        public ServerTests(string Host, int Port)
        {
            host = Host;
            port = Port;
        }

        [Test]
        public void Stop_ServerShouldDisposeTcpListenerResources()
        {
            // Arrange
            IServer server = new Server(host, port);
            var expectedEx = typeof(InvalidOperationException);

            string testMessage = string.Empty;
            TcpClient clientPoint = new TcpClient(host, port);
            IMessenger messenger = new Messenger(clientPoint, (string s) => { testMessage = s; });
            IParticipant participant = new Participant(messenger);

            // Act
            server.Stop();
            var actualEx = Assert.Catch(() => server.ConnectClient());

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType());
        }

        [Test]
        public void Start_TaskShouldRunning()
        {
            // Arrange
            IServer server = new Server(host, port);

            // Act
            server.Start();
            Thread.Sleep(50);

            // Asert
            Assert.IsTrue(server.Status == System.Threading.Tasks.TaskStatus.Running);
            server.Stop();
        }

        [Test]
        public void Stop_TaskShouldBeRanToCompletion()
        {
            // Arrange
            IServer server = new Server(host, port);

            // Act
            server.Start();
            server.Stop();
            Thread.Sleep(50);

            // Asert
            Assert.IsTrue(server.Status == System.Threading.Tasks.TaskStatus.RanToCompletion);
        }

        [Test]
        public void DisconnectClient_SecondCallShouldThrowObjectDisposedException()
        {
            // Arrange
            IServer server = new Server(host, port);
            string testMessage = string.Empty;
            var expectedEx = typeof(ObjectDisposedException);
            TcpClient clientPoint = new TcpClient(host, port);
            IMessenger messenger = new Messenger(clientPoint, (string s) => { testMessage = s; });
            IParticipant participant = new Participant(messenger);

            // Act
            server.Start();
            server.DisconnectClient(clientPoint);
            var actualEx = Assert.Catch(() => server.DisconnectClient(clientPoint));

            // Asert
            Assert.AreEqual(expectedEx, actualEx.GetType());

            server.Stop();
        }

        [Test]
        public void ConnectClient_ServerShouldGetConnection()
        {
            // Arrange
            IServer server = new Server(host, port);
            var expectedEx = typeof(InvalidOperationException);

            string testMessage = string.Empty;
            TcpClient clientPoint = new TcpClient(host, port);
            IMessenger messenger = new Messenger(clientPoint, (string s) => { testMessage = s; });
            IParticipant participant = new Participant(messenger);

            // Act
            server.ConnectClient();

            // Assert
            Assert.AreEqual(1, server.Users.Count);
            Assert.AreEqual(participant.GetType(), server.Users[^1].GetType());

            server.Stop();
        }


        [TestCase(0)]
        [TestCase(1)]
        [TestCase(3)]
        [TestCase(10)]
        [TestCase(40)]
        public void Start_ShouldAddAllClientsToList(int expectedCount)
        {
            // Arrange
            IServer server = new Server(host, port);
            string testMessage = string.Empty;

            // Act
            server.Start();

            for (int i = 0; i < expectedCount; i++)
            {
                TcpClient clientPoint = new TcpClient(host, port);
                IMessenger messenger = new Messenger(clientPoint, (string s) => { testMessage = s; });
                IParticipant participant = new Participant(messenger);
                Thread.Sleep(50);
            }

            // Assert
            Assert.AreEqual(expectedCount, server.Users.Count);
            server.Stop();
        }

        [Test]
        public void DisconnectClient_ShouldCloseClientSocket()
        {
            // Arrange
            IServer server = new Server(host, port);
            string testMessage = string.Empty;
            TcpClient clientPoint = new TcpClient(host, port);
            IMessenger messenger = new Messenger(clientPoint, (string s) => { testMessage = s; });
            IParticipant participant = new Participant(messenger);

            // Act
            server.Start();
            server.DisconnectClient(clientPoint);

            // Assert
            Assert.IsFalse(clientPoint.Connected);

            server.Stop();
        }
    }
    internal class ServerArgsClass
    {
        static object[] FixtureArgs = { new object[] { "127.0.0.1", 1234 } };
    }
}