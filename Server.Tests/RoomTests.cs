﻿using MessageExchange;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;

namespace Server.Tests
{
    [TestFixtureSource(typeof(RoomArgsClass), "FixtureArgs")]
    class RoomTests
    {
        private readonly string host;
        private readonly int port;

        public RoomTests(string Host, int Port)
        {
            host = Host;
            port = Port;
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(10)]
        [TestCase(50)]
        public void AddUser_SingleRoomShouldAddUsersToList(int participantsCount)
        {
            // Arrange
            IAdmin server = new Admin(new Server(host, port));
            server.Start();
            server.CreateRoom("Test Room #0");

            List<IMessenger> participants = new List<IMessenger>();
            for (int i = 0; i < participantsCount; i++)
            {
                TcpClient tcpClient = new TcpClient(host, port);
                IMessenger messenger = new Messenger(tcpClient);
                IParticipant participant = new Participant(messenger);
                participants.Add(participant);
            }

            // Act
            for (int i = 0; i < participants.Count; i++)
            {
                server.Rooms[^1].AddUser(participants[i]);
            }

            // Assert
            Assert.AreEqual(participants.Count, server.Rooms[^1].Participants.Count);
            for (int i = 0; i < server.Rooms[^1].Participants.Count; i++)
            {
                Assert.AreEqual(participants[i], server.Rooms[^1].Participants[i]);
            }

            server.Stop();
        }

        [TestCase(0, 0)]
        [TestCase(1, 0)]
        [TestCase(1, 1)]
        [TestCase(1, 10)]
        [TestCase(10, 10)]
        [TestCase(10, 50)]
        [TestCase(50, 50)]
        public void AddUser_MultipleRoomsShouldAddUsersToList(int roomsCount, int participantsCount)
        {
            // Arrange
            IAdmin server = new Admin(new Server(host, port));
            server.Start();
            for (int i = 0; i < roomsCount; i++)
            {
                server.CreateRoom($"Test Room #{i}");
            }

            List<IMessenger> participants = new List<IMessenger>();
            for (int i = 0; i < participantsCount; i++)
            {
                TcpClient tcpClient = new TcpClient(host, port);
                IMessenger messenger = new Messenger(tcpClient);
                IParticipant participant = new Participant(messenger);
                participants.Add(participant);
            }

            // Act
            for (int i = 0; i < server.Rooms.Count; i++)
            {
                for (int j = 0; j < participants.Count; j++)
                {
                    server.Rooms[i].AddUser(participants[j]);
                }
            }

            // Assert
            Assert.AreEqual(roomsCount, server.Rooms.Count);
            for (int i = 0; i < server.Rooms.Count; i++)
            {
                Assert.AreEqual(participants.Count, server.Rooms[i].Participants.Count);
                for (int j = 0; j < server.Rooms[i].Participants.Count; j++)
                {
                    Assert.AreEqual(participants[j], server.Rooms[i].Participants[j]);
                }
            }

            server.Stop();
        }

        [TestCase(0, 0)]
        [TestCase(1, 0)]
        [TestCase(1, 1)]
        [TestCase(10, 5)]
        [TestCase(50, 30)]
        public void RemoveUser_ShouldRemoveParticipantFromList(int participantsCount, int removedPartCount)
        {
            // Arrange
            List<IMessenger> removedUsers = new List<IMessenger>();
            IAdmin server = new Admin(new Server(host, port));
            server.Start();
            server.CreateRoom("Test Room #0");
            for (int i = 0; i < participantsCount; i++)
            {
                TcpClient tcpClient = new TcpClient(host, port);
                IMessenger messenger = new Messenger(tcpClient);
                IParticipant participant = new Participant(messenger);
                server.Rooms[^1].AddUser(participant);
            }

            // Act
            for (int i = 0; i < removedPartCount; i++)
            {
                removedUsers.Add(server.Rooms[^1].Participants[^1]);
                server.Rooms[^1].RemoveUser(removedUsers[^1]);
            }

            // Assert
            Assert.AreEqual(participantsCount - removedPartCount, server.Rooms[^1].Participants.Count);
            for (int i = 0; i < removedUsers.Count; i++)
            {
                Assert.IsFalse(server.Rooms[^1].Participants.Contains(removedUsers[i]));
            }

            server.Stop();
        }

        [Test]
        public void RemoveUser_ShouldThrowApplicationException()
        {
            // Arrange
            var expectedEx = typeof(System.ApplicationException);
            IAdmin server = new Admin(new Server(host, port));
            server.Start();
            server.CreateRoom("Test Room #0");
            TcpClient tcpClient = new TcpClient(host, port);
            IMessenger messenger = new Messenger(tcpClient);
            IParticipant participant = new Participant(messenger);

            // Act
            var actEx = Assert.Catch(() => server.Rooms[^1].RemoveUser(participant));

            // Assert
            Assert.AreEqual(expectedEx, actEx.GetType());

            server.Stop();
        }

        [TestCase("text with spaces", 1)]
        [TestCase("text with special symbols !@#$%^&*()-+_/", 3)]
        [TestCase("text", 5)]
        [TestCase("", 0)]
        [TestCase("a", 10)]
        [TestCase("123456789", 50)]
        public void Mailing_ShouldSendDataToAllParticipants(string data, int participantsCount)
        {
            // Arrange
            List<string> checkList = new List<string>();
            List<IMessenger> clients = new List<IMessenger>();
            IAdmin server = new Admin(new Server(host, port));
            server.Start();
            server.CreateRoom("Test Room #0");

            for (int i = 0; i < participantsCount; i++)
            {
                TcpClient tcpClient = new TcpClient(host, port);
                IMessenger messenger = new Messenger(tcpClient, (string str) => checkList.Add(str));
                IParticipant participant = new Participant(messenger);
                clients.Add(participant);
            }

            foreach (var item in server.Users)
            {
                server.Rooms[^1].AddUser(item);
            }

            // Act
            server.Rooms[^1].Mailing(data);

            foreach (var item in clients)
            {
                item.ReceiveMessage();
            }

            // Assert
            Assert.AreEqual(participantsCount, checkList.Count);

            foreach (var item in clients)
            {
                item.EndChatting();
            }
            server.Stop();
        }
    }

    internal class RoomArgsClass
    {
        static object[] FixtureArgs = { new object[] { "127.0.0.1", 1234 } };
    }
}
