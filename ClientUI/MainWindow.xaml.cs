﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using MessageExchange;

namespace ClientUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IParticipant participant;
        public MainWindow()
        {
            InitializeComponent();
        }
        private void connectButton_Click(object sender, RoutedEventArgs e)
        {
            TcpClient tcpClient = new TcpClient(hostTextBox.Text, int.Parse(portTextBox.Text));
            IMessenger messenger = new Messenger(tcpClient);
            participant = new Participant(messenger);
            participant.StartReceiving();
            participant.ReceiveMessageAction += PrintMessage;
        }

        private void sendButton_Click(object sender, RoutedEventArgs e)
        {
            participant?.SendMessage(messageTextBox.Text);
        }

        private void PrintMessage(string message) =>
            Dispatcher.BeginInvoke(new ThreadStart(delegate
            {
                messagesListBox.Items.Add($"{message}");
                messagesListBox.ScrollIntoView(messagesListBox.Items[^1]);
            }));

    }
}
